# Algoritmo de clasificación - Perceptrón Multicapa con Keras en Python 3.6

#### Modelo por defecto

![image1](media/ModelDefault.png)

- Solo útiliza cpu


### Parámetros de la red Neuronal

###### Ruta del conjunto de entrenamiento (obtenido del remuestreo) ***se puede omitir al ejecutarlo solo***

-IN_PATH            (/ruta/IND1GEN.bin.csv)

###### Ruta del resultado obtenido para una medida (por defecto .bin)

-OUT_PATH           (/ruta/name.bin)

###### Ruta del conjunto de entrenamiento

-PATH_TRAIN         (/ruta/training_set.csv)

###### Ruta del conjunto de validación (validación/test)

-PATH_TRAIN         (/ruta/validation_set.csv)

###### Número de capas ocultas

-HIDDEN_LAYER        3

###### Número de neuronas para las capas ocultas

-NODES              20

###### Número de clases: Depende del problema

-CLASSES             3

###### Número de epocas para aprender

-EPOCHS              100

###### Tamaño del batch: número de ejemplos presentados para entrenar ** a mayor tamaño más consumo de recursos**

-BATCH               30

###### Tasa de aprendizaje: Permite la convergencia de la red neuronal, debe de ser un valor muy pequeño menor al momentum

-LEARNING_RATE       0.001

###### Evita que la red se atasque en un mínimo local

-MOMENTUM            0.9

###### Función de activación de la capa de entrada

-ACTIVATION_LAYER_INPUT relu

###### Función de activación de la capa oculta

-ACTIVATION_LAYER_OUTPUT    softmax

###### Función de pérdida

-LOSS                  categorical_crossentropy

###### Medida de evaluación durante el entrenamiento

-METRIC                 accuracy

###### Medida de evaluación final

-METRIC_EVALUATION          accuracy


***Los parámetros deben editarse desde el archivo Parametros.txt***