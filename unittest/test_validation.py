import sys
sys.path.append('..') # Adds higher directory to python modules path.
import os
from TABICON37.MySintaxis import *
from MLPerceptron.MLPT import *

def test_neuralnetwork():
#if __name__ == '__main__':

    args = []

    for element in range(1, len(sys.argv)):
        args.append(sys.argv[element].replace('\\','//'))

    param = MyListArgs(args)
    configFile = param.valueArgsAsString('-CONFIG', '')

    if configFile != '':
        param.addArgsFromFile(configFile)

    sintaxis = '-IN_PATH:str -OUT_PATH:str -PATH_TRAIN:str -PATH_VAL:str -HIDDEN_LAYERS:int -NODES:int' \
               ' -CLASSES:int -EPOCHS:int -BATCH:int -LEARNING_RATE:float -MOMENTUM:float' \
               '-ACTIVATION_LAYER_INPUT:str -ACTIVATION_LAYER_OUTPUT:str -LOSS:str -METRIC:str -METRIC_EVALUATION:str' \
               '-CLASS_NOISE:int'

    IN_PATH = param.valueArgsAsString('-IN_PATH', '../data/IND1GEN.bin.csv')
    OUT_PATH = param.valueArgsAsString('-OUT_PATH', '../WORK/IND1GEN.bin')
    PATH_TRAIN = param.valueArgsAsString('-PATH_TRAIN', '../data/iris.csv')
    PATH_VAL = param.valueArgsAsString('-PATH_VAL','../data/iris.csv')
    HIDDEN_LAYERS = param.valueArgsAsInteger('-HIDDEN_LAYERS', 3)
    NODES = param.valueArgsAsInteger('-NODES',20)
    CLASSES = param.valueArgsAsInteger('-CLASSES', 3)
    EPOCHS = param.valueArgsAsInteger('-EPOCHS', 100)
    BATCH = param.valueArgsAsInteger('-BATCH', 30)
    LEARNING_RATE = param.valueArgsAsFloat('-LEARNING_RATE', 0.001)
    MOMENTUM = param.valueArgsAsFloat('-MOMENTUM', 0.9)
    ACTIVATION_LAYER_INPUT = param.valueArgsAsString('-ACTIVATION_LAYER_INPUT','relu')
    ACTIVATION_LAYER_OUTPUT = param.valueArgsAsString('-ACTIVATION_LAYER_OUTPUT','softmax')
    LOSS = param.valueArgsAsString('-LOSS','categorical_crossentropy')
    METRIC = param.valueArgsAsString('-METRIC','accuracy')
    METRIC_EVALUATION = param.valueArgsAsString('-METRIC_EVALUATION','accuracy')
    CLASS_NOISE = param.valueArgsAsInteger('CLASS_NOISE', 3)


    # Functioning

    mlp = MultiLayerPerceptron(in_path=IN_PATH, path_train=PATH_TRAIN, path_val=PATH_VAL, hidden_layers=HIDDEN_LAYERS, nodes=NODES, classes=CLASSES,
                     epochs=EPOCHS, batch=BATCH, learning_rate=LEARNING_RATE, momentum=MOMENTUM,
                     activation_layer_input=ACTIVATION_LAYER_INPUT, activation_layer_output=ACTIVATION_LAYER_OUTPUT, loss=LOSS,
                     metric=METRIC, metric_evaluation=METRIC_EVALUATION, class_noise=CLASS_NOISE)

    # compile model
    mlp.Create_Model()

    # train model
    mlp.Train_model()

    # evaluated model
    mlp.Performance_validation()

    # write score
    score = mlp.Write_score(OUT_PATH)
    #print(score)
    ##### Test #####
    assert  score <= 1, 'unittest passed!'
