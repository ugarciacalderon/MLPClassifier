![image1](media/Portada.png)

# Perceptrón multicapa - Keras

-Modelo por defecto

![image1](media/ModelDefault.png)

Este algoritmo fue desarrollado para técnicas de muestreo y remuestreo.

## Ramas para remuestreo
Estas ramas son útilizadas cuando se propone un método de remuestreo el cual genera *n* clases que la tarea de clasificación requiera.


##### Branch --> Master
Esta rama es utilizada cuando se tienen tres conjuntos de datos (train,validation,test).
* *training set*: entrena al clasificador.
* *validation set*: sirve para obtener una evaluación imparcial del modelo de clasificación entrenado y permite modificar los hiperparametros del algoritmo.
* *test set*: sirve para obtener la evaluación final del conjunto de entrenamiento.


#### Branch --> TenCrossValidation
Esta rama aplica la técnica de ten cross validation sobre el conjunto de entrenamiento, es utilizada cuando solo se tienen dos conjuntos de datos (train,test)


## Ramas para muestreo
Estas ramas son útilizadas cuando se propone un método de muestreo aleatorio, el cual selecciona patrones del conjunto de entrenamiento

##### Branch --> TXVBinaryCoding
Esta rama aplica la técnica de ten cross validation sobre el conjunto de entrenamiento, es utilizada cuando solo se tienen dos conuntos de datos (train,test), contiene
multiples medidas de evaluación

##### Branch --> binary_coding
Esta rama es utilizada cuando se tienen tres conjuntos de datos (train, validation, test).
* *training set*: entrena al clasificador.
* *validation set*: sirve para obtener una evaluación imparcial del modelo de clasificación entrenado y permite modificar los hiperparametros del algoritmo.
* *test set*: sirve para obtener la evaluación final del conjunto de entrenamiento.


### Formato de los archivos recibidos
El método (en cualquier rama) debe recibir dos archivos con el siguiente formato (de lo contrario no funcionará):

### 1.  Archivo del conjunto de datos

![image1](media/irisDataset.png)

    Descripción de las columnas
    - Instance: Esta columna indica el indice de cada patrón, comenzando desde cero hasta el tamaño del conjunto de entrenamiento -1
    - sepal_length,sepal_width,petal_length,petal_width: se refiere a las características del conjunto de entrenamiento
    - Class: Esta columna se refiere a cada una de las clases del problema (iris contiene 3 clases), las clases deben estar codificadas con valores numéricos comenzando desde 0,
      esto depende de las clases del problema, por ejemplo: iris-setosa --> 0, iris-versicolor --> 1, iris-virginica --> 2.
      Para el caso en que por defecto el conjunto de entrenamiento tenga clases numéricas se debe hacer la misma representación, por ejemplo: 1 --> 0, 2 --> 1, 3 --> 2
    - OriginalCoding: Esta columna se refiere a la codificación original del conuunto de entrenamiento (puede ser texto o número).
    -BinaryCoding: Esta columna indica que instancias seran utilizadas durante el entrenamiento (por defecto dejar la columna con 1)

### 2. Archivo adicional

**Cuando se utilizan las ramas de remuestreo:** indicado por el parámetro **-IN_PATH** el archivo pasado en este parámetro debe tener las siguiente columnas (estrictamente):

![image1](media/irisRemuestreo.png)

**Cuando se utilizan las ramas de muestreo:** indicado por el parámetro **-IN_PATH** el archivo pasado en este parámetro debe tener las siguiente columnas (estrictamente):

![image1](media/irisMuestreo.png)

##### Para conocer el funcionamiento de los parámetros del algoritmo de clasificación revise el archivo CONFIG.md
